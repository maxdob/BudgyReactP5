
## POC organic generative growth modeling

### Understanding the algorithm

#### Attractors
* Attractors are focal points that promote growth. The attractors infuence the growth direction of the nodes.

#### Nodes
* Nodes are points through which lines are drawn to render branches. More nodes mean more fluid lines, more nodes also have a negative impact on the performance


## Features

1. Supports both _open_ and _closed_ venation. Configurable via `./core/Defaults.js` or a local `Setting.js` file.
2. Growth can be constrained within _bounding shapes_. See `./core/Path.js` and `./core/Network.js`.
3. _Obstacles_ can be defined that growth must avoid. This visualises negative behaviour just like holes in leaves. See `./core/Path.js` and `./core/Network.js`.
4. Attractors can be placed along the edges of paths, which can in turn be scaled and moved, in order to model _marginal growth_. See `./marginal-growth/js/entry.js`.


## Input values

1. Attraction Distance. Nodes that are within this distance of an attractor will be paired with that attractor. A large distance means more fluent branches. Configurable via `./core/Defaults.js` or a local `Setting.js` file.
2. Kill distance. a attractor will be removed if a node is within this distance. Configurable via `./core/Defaults.js` or a local `Setting.js` file.
3. Segment length. The lenght of nodes as it grows. a long length means more hard corners/curves, but better performance. Configurable via `./core/Defaults.js` or a local `Setting.js` file.
4. Circle radius. Define the size of the circle. Within this circle the growth happens and ends at the edge of the circle. Configurable via a  `./bounds/js/entry.js` 
5. Resolution. How many corners does the shape have. 100 is a circle but 3 would be a triangle. Configurable via a `./bounds/js/entry.js` 
6. Obstacles. Define how many obstacles are within the circle and set the size of these circles. Configurable via `./obstacles/js/entry.js`
7. Canalization. Turn on/off vein thickening. They will progressively thicken from their tips to the roots. configurable via `./core/Defaults.js` or a local `Setting.js` file.


## Implementation notes

See `./core` for common modules:
* `Attractor.js` - location of a single source of auxin growth hormone or other growth-promoting influence
* `Network.js` - manages the growth of nodes based on attractors and provided bounds and obstacles
* `Path.js` - arbitrary path consisting of points, used for either constraining growth ("bounds") or defining areas for growth to avoid ("obstacle").
* `AttractorPatterns.js` - functions for generating attractors arranged in various patterns (grids, noise, etc)
* `Node.js` - a single point in a branch

A couple additional helper modules are also included there:
* `KeyboardInteractions.js` - a structure for handling common keyboard commands that every sketch should have
* `Utilities.js` - small helper functions like `random` and `lerp`
* `ColorPresets.js` - collection of pre-made color palettes for use in `Defaults.js`
* `Defaults.js` - collection of global variables used for configuring the behavior and display of the algorithm
  * Any variable can be overridden on a per-sketch basis using a local `Setting.js` file

## Technologies used
* Native [Canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API), specifically the [CanvasRenderingContext2D](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D) interface, for all drawing
* Vanilla ES6 JavaScript
* Webpack build system with live-reloading dev server

## Packages used
* [KDBush](https://www.npmjs.com/package/kdbush) for KD-tree based spatial index
* [vec2](https://www.npmjs.com/package/vec2) for simple, fast 2D vector math
* [Webpack](https://webpack.js.org/) for modern JS (ES6) syntax, code modularization, bundling, and serving locally.

## Install and run notes
1. Run `npm install` to get all packages
2. Run `npm run serve` to start up Webpack and launch the application in a browser window

### Inspiration:

* [Space colonization](https://github.com/jasonwebb/2d-space-colonization-experiments) in Jason Webb's Morphogenesis Resources repo.
* [Space Colonization Algorithm Part 1](https://bastiaanolij.blogspot.com/2014/12/space-colonization-algorithm-part-1.html) [[Part II](https://bastiaanolij.blogspot.com/2014/12/space-colonization-algorithm-part-2.html)] [[Part III](https://bastiaanolij.blogspot.com/2015/01/space-colonization-algorithm-part-3.html)] by Bastiaan Olij

