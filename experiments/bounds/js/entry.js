import * as Vec2 from 'vec2';
import Network from '../../../core/Network';
import { getRandomAttractors, getGridOfAttractors } from '../../../core/AttractorPatterns';
import Node from '../../../core/Node';
import Path from '../../../core/Path';
import SVGLoader from '../../../core/SVGLoader';
import { random } from '../../../core/Utilities';
import { setupKeyListeners } from '../../../core/KeyboardInteractions';
import Settings from './Settings';

let canvas, ctx;
let network;

const SQUARE = 0;
const CIRCLE = 1;
let currentBoundsShape = CIRCLE;

let showHelp = true;

// Create initial conditions for simulation
let setup = () => {
  // Initialize canvas and context
  canvas = document.getElementById('sketch');
  ctx = canvas.getContext('2d');

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  // Initialize simulation object
  network = new Network(ctx, Settings);

  // Add the bounds, attractors, and root nodes
  resetNetwork();

  // Set up common keyboard interaction listeners
  setupKeyListeners(network);

  // Begin animation loop
  requestAnimationFrame(update);
}

let resetNetwork = () => {
  network.reset();
  addBounds();
  addAttractors();
  addRootNodes();

}

  let addBounds = () => {
    switch(currentBoundsShape) {
      case SQUARE:
        network.bounds = getSquareBounds();
        break;

      case CIRCLE:
        network.bounds = getCircleBounds();
        break;
    }
  }

    // Calculating the square area
    let getSquareBounds = () => {
      const cx = window.innerWidth / 2;
      const cy = window.innerHeight / 2;
      const sideLength = 800;

      return [new Path(
        [
          [cx - sideLength/2, cy - sideLength/2],  // top left corner
          [cx + sideLength/2, cy - sideLength/2],  // top right corner
          [cx + sideLength/2, cy + sideLength/2],  // bottom right corner
          [cx - sideLength/2, cy + sideLength/2]   // bottom left corner
        ],
        'Bounds',
        ctx,
        Settings
      )];
    }

    // Calculating the circle growth area
    let getCircleBounds = () => {
      const cx = window.innerWidth / 2;
      const cy = window.innerHeight / 2;
      const radius = 250;
      const resolution = 100;
      let points = [];

      for(let i = 0; i < resolution; i++) {
        let angle = 2 * Math.PI * i / resolution;
        let x = cx + Math.floor(radius * Math.cos(angle));
        let y = cy + Math.floor(radius * Math.sin(angle));

        points.push([x, y]);
      }

      return [new Path(points, 'Bounds', Settings)];
      // Add ctx to show border of the circle
    }

  let addAttractors = () => {
    // Set up the attractors using pre-made patterns
    let randomAttractors = getRandomAttractors(500, ctx, 10, network.bounds);
    let gridAttractors = getGridOfAttractors(150, 150, ctx, 10, network.bounds);

    network.attractors = gridAttractors;
  }

  // Create the network with initial conditions
  let addRootNodes = () => {
    let branchColors = 'rgb(0,0,0)'; // Choose color of the branches/lines
    
    switch(currentBoundsShape) {
      case SQUARE:
      case CIRCLE:
        // Add a set of random root nodes throughout scene
          network.addNode(
            new Node(
              null,
              new Vec2(
                window.innerWidth/2,
                window.innerHeight/2
              ),
              true,
              ctx,
              Settings,
              branchColors
            )
          );
        break;
    }
  } 

let drawText = () => {

  ctx.fillStyle = 'rgba(0,0,0,5)';

  ctx.fillStyle = 'rgba(255,255,255,1)';
}

// Main program loop
let update = (timestamp) => {
  network.update();
  network.draw();

  if(showHelp) {
    drawText();
  }

  requestAnimationFrame(update);
}



// Kick off the application
setup();