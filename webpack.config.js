const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    bounds: path.resolve('experiments/bounds/js/entry.js'),
    obstacles: path.resolve('experiments/obstacles/js/entry.js'),
  },
  module: {
    rules: [
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      }
    ]
  },
  devtool: 'inline-source-map',
  devServer: {
    host: '127.0.0.1',
    port: 9001,
    publicPath: '/dist/',
    contentBase: path.resolve('./'),
    compress: true,
    open: true,
    watchContentBase: true
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve('dist')
  }
}